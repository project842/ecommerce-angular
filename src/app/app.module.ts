import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SobreComponent } from './navegacao/sobre/sobre.component';
import { MenusComponent } from './menus/menus.component';
import { FooterComponent } from './footer/footer.component';

import { ContatoComponent } from './navegacao/contato/contato.component';
import { HomeComponent } from './navegacao/home/home.component';
import { FeaturesComponent } from './navegacao/features/features.component';
import { ProdutoComponent } from './navegacao/produto/produto.component';
import { ProdutoService } from './navegacao/produto/service/produto.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    SobreComponent,
    MenusComponent,
    FooterComponent,
    ProdutoComponent,
    ContatoComponent,
    HomeComponent,
    FeaturesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    ProdutoService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
